#!/usr/bin/python
import os, sys, random, getpass
from random import randint

lijst = open('/home/' +  getpass.getuser() + '/Documents/woorden.csv')
counter = 0

for l in lijst:
    counter +=1
print('\n\t< Teaching Words >\n\n\tWord Count: %i\n' % counter)

def toets():
    goed = 0
    fout = 0
    counter = 0
    done = []
    questions = []
    goedfout = ''

    for l in open('/home/' + getpass.getuser() + '/Documents/woorden.csv'):
        counter +=1
        questions.append(l)
    try:
        while questions:
            question = random.choice(questions)
            questions.remove(question)
            question = question.split(','); roll = randint(0,1)

            # Progress tracker - If empty, do not show
            if goed or fout:
                print('\t[Right: %i]\t[Wrong: %i]\t The right answere is: %s\n' % (goed, fout, goedfout))

            print('[%i/%i] Question: %s' % (1+goed+fout, counter, question[roll]))
            antwoord = raw_input('[#?] ')

            # Check if given answer is correct
            if roll == 1:
                if not antwoord:
                    fout+=1; goedfout = question[0]
                # No answer means it's the wrong answer
                elif antwoord in question[0]:
                    goed+=1; goedfout = question[0]
                else:
                    fout+=1; goedfout = question[0]
            elif roll == 0:
                if not antwoord:
                    fout+=1; goedfout = question[1]
                # No answer means it's the wrong answer
                elif antwoord in question[1]:
                    goed+=1; goedfout = question[1]
                else:
                    fout+=1; goedfout = question[1]
            else:
                print('\n[ERROR] Could not find the answere > Exiting...\n'); sys.exit(0)
        else:
            print('\nDone!\n'); print('Right answeres: %i\nWrong answeres: %i' % (goed, fout))
    except Exception as e:
        print(e); sys.exit(1)
    except KeyboardInterrupt:
        print('\n'); sys.exit(0)

if __name__ == '__main__':
    toets()
